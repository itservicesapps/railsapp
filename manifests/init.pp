# == Class: railsapp
#
# This is a puppet module that installs Apache + Passenger, using rbenv,
# ruby 2.1.2, rails, and sqlite to run a Ruby on Rails application on
# ubuntu.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*hostname*]
#   The hostname that Apache will create a virtual host for
# [*apprepository*]
#   The git repository that holds the Ruby on Rails application
#
# === Examples
#
#  class { railsapp:
#    hostname       => 'quinn-dev.nam3.msu.edu'
#    apprepository  => 'git@gitlab.msu.edu/itservicesapps/vsscatalog.git
#  }
#
# === Authors
#
# Jeff Quinn <jq@msu.edu>
#
# === Copyright
#
# Copyright 2014 Michigan State University
#
class railsapp ($hostname,$apprepository) {

  #Rails user

  user { 'rails':
      ensure => 'present',
      groups => 'www-data',
      home => '/home/rails',
      managehome => true,
    }
  file { '/home/rails/.ssh':
      ensure      => directory,
      owner       => 'rails',
      group       => 'www-data',
      mode        => '0700',
      require     => User['rails']
  }
  file { '/home/rails/.ssh/id_rsa':
      ensure      => 'present',
      owner       => 'rails',
      group       => 'www-data',
      mode        => '0600',
      source     => 'puppet:///modules/railsapp/itservicesapps-deploy',
      require     => File['/home/rails/.ssh'],
  }
  file { '/home/rails/.ssh/known_hosts':
      ensure    => 'present',
      owner     => 'rails',
      group     => 'www-data',
      mode      => '0600',
      source   => 'puppet:///modules/railsapp/known_hosts',
      require   => File['/home/rails/.ssh'],
  }

#Clone the app to the web directory
vcsrepo { '/home/rails/www':
  ensure    => present,
  provider  => git,
  force     => true,
  user      => 'rails',
  group     => 'www-data',
  source    => "$apprepository",
  require   => [ User['rails'], File['/home/rails/.ssh/known_hosts'],
    File['/home/rails/.ssh/id_rsa'] ],
}

#install rails app
exec { 'install rails app':
  command     =>  'bundle install',
  user        =>  'rails',
  environment =>  ['HOME=/home/rails'],
  path        =>  '/home/rails/.rbenv/shims:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin',
  cwd         =>  '/home/rails/www',
  unless      =>  'bundle check',
#  refreshonly =>  true,
  require     =>  [
    Vcsrepo['/home/rails/www'],
    Rbenv::Install['rails'],
    Rbenv::Compile['2.1.2'],
    Rbenv::Gem['rake'],
    Rbenv::Gem['passenger'],
    Rbenv::Gem['rails'],
    Rbenv::Gem['sqlite3'],
  ],
  notify      =>  Exec['setup database'],
}

exec { 'setup database':
  command     => 'bundle exec rake db:setup RAILS_ENV="production"',
  user        => 'rails',
  path        =>  '/home/rails/.rbenv/shims:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin',
  cwd         => '/home/rails/www',
#  refreshonly => true,
  require     => Exec['install rails app'],
}


#rbenv, requires rails user
  rbenv::install { "rails":
    require => User['rails'],
    group   => 'www-data',
    home    => '/home/rails',
  }

  rbenv::compile { "2.1.2":
    user     => 'rails',
    home     => '/home/rails',
    global   => true,
    require => Rbenv::Install['rails'],
  }

  rbenv::gem { "passenger":
    user  => 'rails',
    ruby  => '2.1.2',
    require => Rbenv::Compile['2.1.2'],
  }

  rbenv::gem { "sqlite3":
    user  => 'rails',
    ruby  => '2.1.2',
    require => Rbenv::Compile['2.1.2'],
  }

  rbenv::gem { "rake":
    user  => 'rails',
    ruby  => '2.1.2',
    require => Rbenv::Compile['2.1.2'],
  }

  rbenv::gem { "rails":
    user  => 'rails',
    ruby  => '2.1.2',
    require => Rbenv::Compile['2.1.2'],
  }

#apache
  class { 'apache':
    default_confd_files => false,
  }

  apache::vhost { "$hostname non-ssl":
    servername  => "$hostname",
    #require =>  Exec['install apache passenger mod'],
    port    => '80',
    docroot => '/home/rails/www/public',
    docroot_owner => 'rails',
    docroot_group => 'www-data',
    rewrites      => [
      {
        comment   => 'redirect non-ssl traffic to ssl',
        rewrite_cond  =>  ['%{HTTPS} off'],
        rewrite_rule  => ['^/?(.*) https://%{SERVER_NAME}/$1 [R,L]'],
      }
    ]
  }

  apache::vhost { "$hostname ssl":
    servername  => "$hostname",
    port    => '443',
    #require =>  Exec['install apache passenger mod'],
    docroot => '/home/rails/www/public',
    docroot_owner => 'rails',
    docroot_group => 'www-data',
    ssl           => true,
    directories   => [
      { path      => '/home/rails/www/public',
        passenger_enabled => 'on',
        options   => ['-MultiViews'],
      },
    ]
  }

#apache mod passenger
  package { ['libsqlite3-dev', 'nodejs', 'sqlite3', 'libcurl4-openssl-dev',
            'apache2-threaded-dev', 'libapr1-dev','libaprutil1-dev',
            'libapache2-mod-passenger']:
  }


#  exec { 'install apache passenger mod':
#    command     =>  '/home/rails/.rbenv/shims/passenger-install-apache2-module -a',
#    user        =>  'root',
#    environment =>  ['HOME=/home/rails'],
#    path        =>  '/home/rails/.rbenv/shims:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin',
#    cwd         =>  '/home/rails',
#    require     =>  [
#      User['rails'],
#      Vcsrepo['/home/rails/www'],
#      Rbenv::Install['rails'],
#      Rbenv::Compile['2.1.2'],
#      Rbenv::Gem['rake'],
#      Rbenv::Gem['passenger'],
#      Rbenv::Gem['rails'],
#      Rbenv::Gem['sqlite3'],
#      Package['libsqlite3-dev'],
#      Package['sqlite3'],
#      Package['libcurl4-openssl-dev'],
#      Package['apache2-threaded-dev'],
#      Package['libapr1-dev'],
#      Package['libaprutil1-dev'],
#    ],
#  }

  class { 'apache::mod::passenger':
#    passenger_root  => '/home/rails/.rbenv/versions/2.1.2/lib/ruby/gems/2.1.0/gems/passenger-4.0.45',
    passenger_ruby  => '/home/rails/.rbenv/shims/ruby',
    passenger_default_ruby  => '/home/rails/.rbenv/shims/ruby',
    require         => Package['libapache2-mod-passenger'],
#    mod_path        => '/home/rails/.rbenv/versions/2.1.2/lib/ruby/gems/2.1.0/gems/passenger-4.0.45/buildout/apache2/mod_passenger.so',
#    require         => Exec['install apache passenger mod'],
  }

}
